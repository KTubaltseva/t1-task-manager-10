package ru.t1.ktubaltseva.tm.api;

import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task create(String name, String description);

}
