package ru.t1.ktubaltseva.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void displayTasks();

}
