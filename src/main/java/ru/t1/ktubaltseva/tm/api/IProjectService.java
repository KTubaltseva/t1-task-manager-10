package ru.t1.ktubaltseva.tm.api;

import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project create(String name, String description);

}
