package ru.t1.ktubaltseva.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void displayProjects();

}
