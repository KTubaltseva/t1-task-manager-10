package ru.t1.ktubaltseva.tm.api;

import ru.t1.ktubaltseva.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
