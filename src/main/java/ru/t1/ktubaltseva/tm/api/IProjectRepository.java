package ru.t1.ktubaltseva.tm.api;

import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

}
