package ru.t1.ktubaltseva.tm.model;

import java.util.UUID;

public final class Project {

    private final String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        result += "Id: " + id;
        if (name != null)
            result += ", Name: " + name;
        if (description != null)
            result += ", Desc: " + description;
        return result;
    }

}
