package ru.t1.ktubaltseva.tm.controller;

import ru.t1.ktubaltseva.tm.api.ITaskController;
import ru.t1.ktubaltseva.tm.api.ITaskService;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine() ;
        Task task = taskService.create(name, description);
        if (task == null) System.err.println("ERROR");
        else {
            System.out.println(task);
            System.out.println("[OK]");
        }
    }

    @Override
    public void clearTasks(){
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void displayTasks(){
        System.out.println("[DISPLAY TASKS]");
        int index = 1;
        List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

}
