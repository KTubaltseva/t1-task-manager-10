package ru.t1.ktubaltseva.tm.controller;

import ru.t1.ktubaltseva.tm.api.IProjectController;
import ru.t1.ktubaltseva.tm.api.IProjectService;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine() ;
        Project project = projectService.create(name, description);
        if (project == null) System.err.println("ERROR");
        else {
            System.out.println(project);
            System.out.println("[OK]");
        }
    }

    @Override
    public void clearProjects(){
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void displayProjects(){
        System.out.println("[DISPLAY PROJECTS]");
        int index = 1;
        List<Project> projects = projectService.findAll();
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

}
