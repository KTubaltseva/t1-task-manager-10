package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.ICommandRepository;
import ru.t1.ktubaltseva.tm.api.ICommandService;
import ru.t1.ktubaltseva.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
