package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.IProjectService;
import ru.t1.ktubaltseva.tm.api.IProjectRepository;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project){
        if (project == null) return null;
        projectRepository.add(project);
        return project;
    }

    @Override
    public void clear(){
        projectRepository.clear();
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Project project = add(new Project(name, description));
        return project;
    }

    @Override
    public List<Project> findAll(){
        return projectRepository.findAll();
    }

}
