package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.ITaskService;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task){
        if (task == null) return null;
        taskRepository.add(task);
        return task;
    }

    @Override
    public void clear(){
        taskRepository.clear();
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = add(new Task(name, description));
        return task;
    }

    @Override
    public List<Task> findAll(){
        return taskRepository.findAll();
    }

}
